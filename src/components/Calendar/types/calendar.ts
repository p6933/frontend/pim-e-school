import { View } from "@syncfusion/ej2-react-schedule";

export type CalendarProps = {
  services: any[];
  option: View;
  currentView: View;
  views: View[];
  height?: string;
  title?: string;
};
