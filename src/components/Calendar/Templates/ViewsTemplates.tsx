import styles from "./templates.module.scss";

export const eventTemplate = (props) => {
  return (
    <>
      <div
        className={styles.templateWrap}
        style={{ color: props.PrimaryColor }}
      >
        {props.Subject}
      </div>
      {/* <div>{new Date(props.StartTime)}</div> */}
      {/* <div>{new Date(props.EndTime)}</div> */}
      <div className={styles.templateWrap}>{props.Location}</div>
    </>
  );
};
