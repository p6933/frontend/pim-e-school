import React from "react";
import {
  Inject,
  ScheduleComponent,
  ViewDirective,
} from "@syncfusion/ej2-react-schedule";
import { exampleCalendarData } from "./exampleCalendarData";
import "./Templates/templates.scss";
import styles from "./calendar.module.scss";
import { CalendarProps } from "./types/calendar";


const Calendar = ({ services, option, currentView, height, title, views}: CalendarProps) => {
  return (
    <div className={styles.container}>
        {title && <h2 className={styles.title}>{title}</h2>}
      <ScheduleComponent
        eventSettings={{ dataSource: exampleCalendarData }}
        currentView={currentView}
        height={height}
        views={views}
      >
        <ViewDirective option={option}></ViewDirective>
        <Inject services={services} />
      </ScheduleComponent>
    </div>
  );
};

export default Calendar;
