// new Date(2022, 1, 1, 4, 45)
// year, month, day, hour, min

export const exampleCalendarData = [
  {
    Id: 1,
    StartTime: new Date(2022, 1, 2, 4, 30),
    EndTime: new Date(2022, 1, 2, 6, 30),
    Subject: "4DATAVIZ",
    Location: "Télétravail",
    PrimaryColor: "#FAC35C",
  },
  {
    Id: 2,
    StartTime: new Date(2022, 1, 1, 4, 45),
    EndTime: new Date(2022, 1, 1, 6, 10),
    Subject: "4FULLBK",
    Location: "Présentiel",
    PrimaryColor: "#3BF582",
  },
  {
    Id: 3,
    StartTime: new Date(2022, 1, 7, 4, 30),
    EndTime: new Date(2022, 1, 7, 6, 30),
    Subject: "4DATAVIZ",
    Location: "Télétravail",
    PrimaryColor: "#FAC35C",
  },
  {
    Id: 4,
    StartTime: new Date(2022, 1, 28, 4, 30),
    EndTime: new Date(2022, 1, 28, 6, 30),
    Subject: "4DATAVIZ",
    Location: "Télétravail",
    PrimaryColor: "#FAC35C",
  },
];
