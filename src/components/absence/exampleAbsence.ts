export const exampleAbsence = [
    {
      subject: "Javascript",
      startTime: new Date(2022, 1, 2, 4, 30),
      intervenant: 'John Doe',
    },
    {
      subject: "Nodejs",
      startTime: new Date(2022, 1, 2, 4, 30),
      intervenant: 'Jeanne Doe',
    },
    {
      subject: "Java",
      startTime: new Date(2022, 1, 2, 4, 30),
      intervenant: 'Dummy Doe',
    },
  ];
  