import React from "react";
import { format } from "../../utils/format";
import styles from "./absence.module.scss";

type Absence = { subject: string; startTime: Date; intervenant: string };

type AbsenceProps = {
  absences?: Absence[];
  title?: string;
};

const AbsenceList = ({ absences, title }: AbsenceProps) => {
  return (
    <>
      <div>
        {title && <h2 className={styles.title}>{title}</h2>}
        <div className={styles.gridAbsences}>
          <span>Subject</span>
          <span>Date</span>
          <span>Intervenant</span>
        </div>
        {absences
          ? absences.map((absence, index) => {
              return (
                <div key={index} className={styles.gridAbsences}>
                  <div className={styles.head}>{absence?.subject}</div>
                  <div className={styles.head}>
                    {format(absence?.startTime)}
                  </div>
                  <div className={styles.head}>{absence?.intervenant}</div>
                </div>
              );
            })
          : "Loading..."}
      </div>
    </>
  );
};

export default AbsenceList;
