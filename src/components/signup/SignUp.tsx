import React from "react";
import { Field, Form } from "react-final-form";
import { useNavigate } from "react-router-dom";

import styles from "./signup.module.scss";

type Props = {
  onSubmit: any;
  setUserData: any;
};

const SignUp = (props: Props) => {
  const navigate = useNavigate();
  return (
    <>
      <Form onSubmit={props.onSubmit}>
        {(props) => (
          <form onSubmit={props.handleSubmit} className={styles.form}>
            <h2>Sign up</h2>

            <label className={styles.label} htmlFor="email">
              email
            </label>
            <Field name="email">
              {(props) => (
                <div>
                  <input {...props.input} />
                </div>
              )}
            </Field>

            <label className={styles.label} htmlFor="firstname">
              firstname
            </label>
            <Field name="firstname">
              {(props) => (
                <div>
                  <input {...props.input} />
                </div>
              )}
            </Field>

            <label className={styles.label} htmlFor="lastname">
              lastname
            </label>
            <Field name="lastname">
              {(props) => (
                <div>
                  <input {...props.input} />
                </div>
              )}
            </Field>

            <label className={styles.label} htmlFor="password">
              password
            </label>
            <Field name="password" type="password">
              {(props) => (
                <div>
                  <input {...props.input} />
                </div>
              )}
            </Field>
            <span onClick={() => navigate("/signin")}>
              Already have an account ? Sign up here !
            </span>
            <button className={styles.btn} type="submit">
              Submit
            </button>
          </form>
        )}
      </Form>
    </>
  );
};

export default SignUp;
