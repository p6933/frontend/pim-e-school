import React from "react";
import { format } from "../../utils/format";
import styles from "./notes.module.scss";

type Note = {
  subject: string;
  score: number;
  startTime: Date;
  intervenant: string;
};

type NotesProps = {
  notes?: Note[];
  title?: string;
};

const NotesList = ({ notes, title }: NotesProps) => {
  return (
    <div>
      {title && <h2 className={styles.title}>{title}</h2>}
      <div className={styles.gridNotes}>
        <span>Subject</span>
        <span>Score</span>
        <span>Date</span>
        <span>Intervenant</span>
      </div>
      {notes
        ? notes.map((note, index) => {
            return (
              <div key={index} className={styles.gridNotes}>
                <div className={styles.head}>{note?.subject}</div>
                <div className={styles.head}>{note?.score}</div>
                <div className={styles.head}>{format(note?.startTime)}</div>
                <div className={styles.head}>{note?.intervenant}</div>
              </div>
            );
          })
        : "Loading..."}
    </div>
  );
};

export default NotesList;
