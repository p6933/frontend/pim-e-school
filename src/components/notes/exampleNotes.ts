export const exampleNotes = [
  {
    subject: "Javascript",
    score: 18.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'John Doe',
  },
  {
    subject: "Nodejs",
    score: 19.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Jeanne Doe',
  },
  {
    subject: "Java",
    score: 15.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Dummy Doe',
  },
  {
    subject: "Agile",
    score: 13.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Agility Master Doe',
  },
  {
    subject: "Malifood",
    score: 20,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Poulet Yassaaaa',
  },
];
