import React from "react";
import { Field, Form } from "react-final-form";
import { useNavigate } from "react-router-dom";

import styles from "./signin.module.scss";

type Props = {
  onSubmit: any;
  setUserData: any;
};

const SignIn = (props: Props) => {
  const navigate = useNavigate();
  return (
    <>
      <Form onSubmit={props.onSubmit}>
        {(props) => (
          <form onSubmit={props.handleSubmit} className={styles.form}>
            <h2>Sign in</h2>

            <label className={styles.label} htmlFor="email">
              email
            </label>
            <Field name="email">
              {(props) => (
                <div>
                  <input {...props.input} />
                </div>
              )}
            </Field>

            <label className={styles.label} htmlFor="password">
              password
            </label>
            <Field name="password" type="password">
              {(props) => (
                <div>
                  <input {...props.input} />
                </div>
              )}
            </Field>
            <span onClick={() => navigate("/signup")}>
              No account ? click here to create an account !
            </span>
            <button className={styles.btn} type="submit">
              Submit
            </button>
          </form>
        )}
      </Form>
    </>
  );
};

export default SignIn;
