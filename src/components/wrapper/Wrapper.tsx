import React from "react";

import styles from "./wrapper.module.scss";

type Props = {
  children?: any;
};

const Wrapper = (props: Props) => {
  return <div className={styles.wrapper}>{props.children}</div>;
};

export default Wrapper;
