import React from "react";
import { Link } from "react-router-dom";
import styles from "./navbar.module.scss";

const Navbar = () => {
  return (
    <>
      <div className={styles.navbar}>
        <div className={styles.leftblock}>logo</div>
        <div className={styles.rightblock}>
            <Link to="/" className={styles.link}>Home</Link>
            <Link to="/about" className={styles.link}>About</Link>
            <Link to="/calendar" className={styles.link}>Calendar</Link>
            <Link to="/signin" className={styles.link}>Sign in</Link>
        </div>
      </div>
    </>
  );
};

export default Navbar;
