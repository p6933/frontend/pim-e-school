import {
  BrowserRouter as Router,
  Routes as Switch,
  Route,
} from "react-router-dom";
import AbsencesPage from "./pages/AbsencesPage";
import CalendarPage from "./pages/CalendarPage";
import Home from "./pages/Home";
import NotesPage from "./pages/NotesPage";
import SigninPage from "./pages/SigninPage";
import SignupPage from "./pages/SignupPage";
import StudentInterface from "./pages/StudentInterface";

const routes = (
  <>
    <Route index element={<Home />} />
    <Route path="/signup" element={<SignupPage />} />
    <Route path="/signin" element={<SigninPage />} />
    <Route path="/calendar" element={<CalendarPage />} />
    <Route path="/student" element={<StudentInterface />} />
    <Route path="/student/notes" element={<NotesPage />} />
    <Route path="/student/absences" element={<AbsencesPage />} />
  </>
);

function App() {
  return (
    <>
      <Router>
        <Switch>{routes}</Switch>
      </Router>
    </>
  );
}

export default App;
