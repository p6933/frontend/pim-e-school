export const format = (date) => {
    const options = {year: 'numeric', month: 'long', day: 'numeric'} 
    return new Date().toLocaleDateString([], options)
}