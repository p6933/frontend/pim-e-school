import { Week, WorkWeek } from "@syncfusion/ej2-react-schedule";
import React, { useEffect, useState } from "react";
import AbsenceList from "../components/absence/AbsenceList";
import { exampleAbsence } from "../components/absence/exampleAbsence";
import Calendar from "../components/Calendar/Calendar";
import Navbar from "../components/navbar/Navbar";
import { exampleNotes } from "../components/notes/exampleNotes";
import NotesList from "../components/notes/NotesList";
import Grid3 from "../components/wrapper/Grid3";
import Wrapper from "../components/wrapper/Wrapper";

const StudentInterface = () => {
  const [notes, setNotes] = useState<any>();

  const filterNotes = () => {
    const copyNotes = [...exampleNotes];
    const el1 = copyNotes.pop();
    const el2 = copyNotes.pop();
    const el3 = copyNotes.pop();
    const filteredNotes = [el1, el2, el3];
    setNotes(filteredNotes);
    return filteredNotes;
  };

  useEffect(() => {
    filterNotes();
  }, []);

  return (
    <>
      <Navbar />
      <Wrapper>
        <Grid3>
          <Calendar
            views={["WorkWeek", "Week"]}
            title="Week Calendar"
            services={[WorkWeek, Week]}
            option={"Week"}
            currentView={"Week"}
            height="400px"
          />
          <NotesList title="Recent Notes" notes={notes} />
          <AbsenceList title="Recent absences" absences={exampleAbsence} />
        </Grid3>
      </Wrapper>
    </>
  );
};

export default StudentInterface;
