import React, { useState } from "react";
import Navbar from "../components/navbar/Navbar";
import SignIn from "../components/signin/SignIn";
import Wrapper from "../components/wrapper/Wrapper";

type signin = {
  email: string;
  password: string;
};

const SigninPage = () => {
  const [userData, setUserData] = useState();

  const login = async (formValues: signin) => {
    try {
      console.log(formValues);
      // api call here
      // await api.method(userData)
      //toaster success
    } catch (error) {
      console.log(error);
      //toaster error
    }
  };

  return (
    <>
      <Navbar />
      <Wrapper>
        <SignIn onSubmit={login} setUserData={setUserData} />
      </Wrapper>
    </>
  );
};

export default SigninPage;
