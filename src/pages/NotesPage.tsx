import React, { useState } from 'react'
import Navbar from '../components/navbar/Navbar'
import { exampleNotes } from '../components/notes/exampleNotes'
import NotesList from '../components/notes/NotesList'
import Wrapper from '../components/wrapper/Wrapper'

const NotesPage = () => {
  const [allNotes,] = useState(exampleNotes);
  

  return (
    <>
        <Navbar />
        <Wrapper>
            {allNotes && <NotesList notes={allNotes} />}
        </Wrapper>
    </>
  )
}

export default NotesPage