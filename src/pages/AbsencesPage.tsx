import React from 'react'
import AbsenceList from '../components/absence/AbsenceList'
import { exampleAbsence } from '../components/absence/exampleAbsence'
import Navbar from '../components/navbar/Navbar'
import Wrapper from '../components/wrapper/Wrapper'

const AbsencesPage = () => {
  return (
    <>
        <Navbar />
        <Wrapper>
            <AbsenceList absences={exampleAbsence} />
        </Wrapper>
    </>
  )
}

export default AbsencesPage