import {
  Agenda,
  Day,
  DragAndDrop,
  Month,
  Resize,
  Week,
  WorkWeek,
} from "@syncfusion/ej2-react-schedule";
import React from "react";
import Calendar from "../components/Calendar/Calendar";
import Navbar from "../components/navbar/Navbar";
import Wrapper from "../components/wrapper/Wrapper";

const CalendarPage = () => {
  return (
    <>
      <Navbar />
      <Wrapper>
        <Calendar
          services={[Day, Week, WorkWeek, Month, Agenda, DragAndDrop, Resize]}
          option={"Month"}
          currentView={"Month"}
          views={["Week", "Day", "Month", "Agenda", "WorkWeek"]}
        />
      </Wrapper>
    </>
  );
};

export default CalendarPage;
