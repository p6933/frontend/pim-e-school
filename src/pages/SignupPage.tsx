import React, { useState } from "react";
import Navbar from "../components/navbar/Navbar";
import SignUp from "../components/signup/SignUp";
import Wrapper from "../components/wrapper/Wrapper";

type signup = {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
};

const SignupPage = () => {
  const [userData, setUserData] = useState();

  const createAccount = async (formValues: signup) => {
    try {
      console.log(formValues);
      // api call here
      // await api.method(userData)
      //toaster success
    } catch (error) {
      console.log(error);
      //toaster error
    }
  };

  return (
    <>
      <Navbar />
      <Wrapper>
        <SignUp onSubmit={createAccount} setUserData={setUserData} />
      </Wrapper>
    </>
  );
};

export default SignupPage;
